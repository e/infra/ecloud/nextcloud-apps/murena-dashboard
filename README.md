## Make murena-dashboard app as default dashboard

- To make default dashboard, add `'defaultapp' => 'murena-dashboard'` changes in `/config/config.php`. 

## Config options

- Set `shop_referral_program_url` to your referral program url in config.php to activate referral program section 
